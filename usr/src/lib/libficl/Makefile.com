#
# This file and its contents are supplied under the terms of the
# Common Development and Distribution License ("CDDL"), version 1.0.
# You may only use this file in accordance with the terms of version
# 1.0 of the CDDL.
#
# A full copy of the text of the CDDL should have accompanied this
# source.  A copy of the CDDL is also available via the Internet at
# http://www.illumos.org/license/CDDL.
#

#
# Copyright 2016 Toomas Soome <tsoome@me.com>
#

LIBRARY=libficl-sys.a
MAJOR = 4
MINOR = 1.0
VERS=.$(MAJOR).$(MINOR)

OBJECTS= dictionary.o system.o fileaccess.o float.o double.o prefix.o search.o \
	softcore.o stack.o tools.o vm.o primitives.o unix.o utility.o \
	hash.o callback.o word.o loader.o pager.o extras.o \
	loader_emu.o lz4.o

include $(SRC)/lib/Makefile.lib

LIBS=	$(DYNLIB) $(LINTLIB)

FICLDIR=	$(SRC)/common/ficl
C99MODE=	$(C99_ENABLE)
CPPFLAGS +=	-I.. -I$(FICLDIR) -D_LARGEFILE64_SOURCE=1

LDLIBS +=	-lc -lm -lumem

HEADERS= ficl.h ficltokens.h ficllocal.h ficlplatform/unix.h

VPATH=	../softcore:$(FICLDIR):$(FICLDIR)/ficlplatform:$(FICLDIR)/emu:$(FICLDIR)/softcore

$(LINTLIB) := SRCS=	../$(LINTSRC)

all: $(LIBS)

include $(SRC)/lib/Makefile.targ
