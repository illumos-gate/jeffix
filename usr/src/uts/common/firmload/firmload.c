/*	$NetBSD: firmload.c,v 1.19 2014/03/25 16:19:13 christos Exp $	*/

/*
 * Copyright 2014 Hans Rosenfeld <rosenfeld@grumpf.hope-2000.org>
 */

/*-
 * Copyright (c) 2005, 2006 The NetBSD Foundation, Inc.
 * All rights reserved.
 *
 * This code is derived from software contributed to The NetBSD Foundation
 * by Jason R. Thorpe.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * The firmload API provides an interface for device drivers to access
 * firmware images that must be loaded onto their devices.
 */

#include <sys/param.h>
#include <sys/fcntl.h>
#include <sys/systm.h>
#include <sys/vnode.h>
#include <sys/lwp.h>
#include <sys/file.h>
#include <sys/cmn_err.h>
#include <sys/modctl.h>

#include <sys/firmload.h>

struct firmware_handle {
	struct vnode	*fh_vp;
	off_t		 fh_size;
};

static firmware_handle_t
firmware_handle_alloc(void)
{

	return (kmem_alloc(sizeof (struct firmware_handle), KM_SLEEP));
}

static void
firmware_handle_free(firmware_handle_t fh)
{

	kmem_free(fh, sizeof (struct firmware_handle));
}

#if !defined(FIRMWARE_PATHS)
#define	FIRMWARE_PATHS		\
	"/kernel/firmware"
#endif

static char firmware_paths[MAXPATHLEN+1] = FIRMWARE_PATHS;

static char *
firmware_path_next(const char *drvname, const char *imgname, char *pnbuf,
    char **prefixp)
{
	char *prefix = *prefixp;
	size_t maxprefix, i;

	if (prefix == NULL		/* terminated early */
	    || *prefix == '\0'		/* no more left */
	    || *prefix != '/') {	/* not absolute */
		*prefixp = NULL;
	    	return (NULL);
	}

	/*
	 * Compute the max path prefix based on the length of the provided
	 * names.
	 */
	maxprefix = MAXPATHLEN -
		(1 /* / */
		 + strlen(drvname)
		 + 1 /* / */
		 + strlen(imgname)
		 + 1 /* terminating NUL */);

	/* Check for underflow (size_t is unsigned). */
	if (maxprefix > MAXPATHLEN) {
		*prefixp = NULL;
		return (NULL);
	}

	for (i = 0; i < maxprefix; i++) {
		if (*prefix == ':' || *prefix == '\0')
			break;
		pnbuf[i] = *prefix++;
	}

	if (*prefix != ':' && *prefix != '\0') {
		/* Path prefix was too long. */
		*prefixp = NULL;
		return (NULL);
	}

	if (*prefix != '\0')
		prefix++;
	*prefixp = prefix;

	ASSERT(MAXPATHLEN >= i);
	snprintf(pnbuf + i, MAXPATHLEN - i, "/%s/%s", drvname, imgname);

	return (pnbuf);
}

static char *
firmware_path_first(const char *drvname, const char *imgname, char *pnbuf,
    char **prefixp)
{

	*prefixp = firmware_paths;
	return (firmware_path_next(drvname, imgname, pnbuf, prefixp));
}

/*
 * firmware_open:
 *
 *	Open a firmware image and return its handle.
 */
int
firmware_open(const char *drvname, const char *imgname, firmware_handle_t *fhp)
{
	struct vattr va;
	char *pnbuf, *path, *prefix;
	firmware_handle_t fh;
	struct vnode *vp;
	int error;

	if (drvname == NULL || imgname == NULL)
		return (EINVAL);

	pnbuf = kmem_alloc(MAXPATHLEN+1, KM_SLEEP);

	fh = firmware_handle_alloc();
	ASSERT(fh != NULL);

	error = 0;
	for (path = firmware_path_first(drvname, imgname, pnbuf, &prefix);
	     path != NULL;
	     path = firmware_path_next(drvname, imgname, pnbuf, &prefix)) {
		error = vn_openat(path, UIO_SYSSPACE, FREAD, 0, &vp, 0, 0,
		    rootdir, -1);
		if (error == ENOENT) {
			continue;
		}
		break;
	}

	if (error) {
		firmware_handle_free(fh);
		return (error);
	}

	error = VOP_GETATTR(vp, &va, 0, kcred, NULL);
	if (error) {
		(void)VOP_CLOSE(vp, FREAD, 1, (offset_t)0, kcred, NULL);
		firmware_handle_free(fh);
		return (error);
	}

	if (va.va_type != VREG) {
		(void)VOP_CLOSE(vp, FREAD, 1, (offset_t)0, kcred, NULL);
		firmware_handle_free(fh);
		return (EINVAL);
	}

	/* XXX Mark as busy text file. */

	fh->fh_vp = vp;
	fh->fh_size = va.va_size;

	*fhp = fh;
	return (0);
}

/*
 * firmware_close:
 *
 *	Close a firmware image.
 */
int
firmware_close(firmware_handle_t fh)
{
	int error;

	error = VOP_CLOSE(fh->fh_vp, FREAD, 1, (offset_t)0, kcred, NULL);
	firmware_handle_free(fh);
	return (error);
}

/*
 * firmware_get_size:
 *
 *	Return the total size of a firmware image.
 */
off_t
firmware_get_size(firmware_handle_t fh)
{

	return (fh->fh_size);
}

/*
 * firmware_read:
 *
 *	Read data from a firmware image at the specified offset into
 *	the provided buffer.
 */
int
firmware_read(firmware_handle_t fh, off_t offset, void *buf, size_t len)
{

	return (vn_rdwr(UIO_READ, fh->fh_vp, buf, len, offset,
			UIO_SYSSPACE, 0, 0, kcred, NULL));
}

/*
 * firmware_malloc:
 *
 *	Allocate a firmware buffer of the specified size.
 *
 *	NOTE: This routine may block.
 */
void *
firmware_malloc(size_t size)
{

	return (kmem_alloc(size, KM_SLEEP));
}

/*
 * firmware_free:
 *
 *	Free a previously allocated firmware buffer.
 */
/*ARGSUSED*/
void
firmware_free(void *v, size_t size)
{

	kmem_free(v, size);
}


/*
 * modlinkage
 */
static struct modlmisc firmload_modlmisc = {
	&mod_miscops,
	"Firmware loader API"
};

static struct modlinkage firmload_modlinkage = {
	MODREV_1,
	&firmload_modlmisc,
	NULL
};

int
_init(void)
{
	return (mod_install(&firmload_modlinkage));
}

int
_fini(void)
{
	return (mod_remove(&firmload_modlinkage));
}

int
_info(struct modinfo *modinfop)
{
	return (mod_info(&firmload_modlinkage, modinfop));
}
