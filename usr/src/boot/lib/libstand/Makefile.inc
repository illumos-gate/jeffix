#
# This file and its contents are supplied under the terms of the
# Common Development and Distribution License ("CDDL"), version 1.0.
# You may only use this file in accordance with the terms of version
# 1.0 of the CDDL.
#
# A full copy of the text of the CDDL should have accompanied this
# source.  A copy of the CDDL is also available via the Internet at
# http://www.illumos.org/license/CDDL.
#

#
# Copyright 2016 Toomas Soome <tsoome@me.com>
#

#
# Notes:
# - We don't use the libc strerror/sys_errlist because the string table is
#   quite large.
#

# standalone components and stuff we have modified locally
SRCS=	gzguts.h zutil.h __main.c assert.c bcd.c bswap.c environment.c \
	getopt.c gets.c globals.c pager.c printf.c strdup.c strerror.c \
	strtol.c strtoul.c random.c sbrk.c twiddle.c zalloc.c zalloc_malloc.c

OBJS=	__main.o assert.o bcd.o bswap.o environment.o \
	getopt.o gets.o globals.o pager.o printf.o \
	strdup.o strerror.o strtol.o strtoul.o random.o \
	sbrk.o twiddle.o zalloc.o zalloc_malloc.o

# private (pruned) versions of libc string functions
SRCS +=	strcasecmp.c
OBJS +=	strcasecmp.o

# from libc
SRCS += ntoh.c
OBJS += ntoh.o

# string functions from libc
SRCS +=	bcmp.c bcopy.c bzero.c ffs.c fls.c \
	memccpy.c memchr.c memcmp.c memcpy.c memmove.c memset.c \
	qdivrem.c strcat.c strchr.c strcmp.c strcpy.c \
	strcspn.c strlcat.c strlcpy.c strlen.c strncat.c strncmp.c strncpy.c \
	strpbrk.c strrchr.c strsep.c strspn.c strstr.c strtok.c swab.c
OBJS +=	bcmp.o bcopy.o bzero.o ffs.o fls.o \
	memccpy.o memchr.o memcmp.o memcpy.o memmove.o memset.o \
	qdivrem.o strcat.o strchr.o strcmp.o strcpy.o \
	strcspn.o strlcat.o strlcpy.o strlen.o strncat.o strncmp.o strncpy.o \
	strpbrk.o strrchr.o strsep.o strspn.o strstr.o strtok.o swab.o

# uuid functions from libc
SRCS += uuid_create_nil.c uuid_equal.c uuid_from_string.c uuid_is_nil.c \
	uuid_to_string.c
OBJS += uuid_create_nil.o uuid_equal.o uuid_from_string.o uuid_is_nil.o \
	uuid_to_string.o

# decompression functionality from libbz2
# NOTE: to actually test this functionality after libbz2 upgrade compile
# loader(8) with LOADER_BZIP2_SUPPORT defined
CFLAGS += -DBZ_NO_STDIO -DBZ_NO_COMPRESS
SRCS +=	libstand_bzlib_private.h

SRCS +=	_bzlib.c _crctable.c _decompress.c _huffman.c _randtable.c
OBJS +=	_bzlib.o _crctable.o _decompress.o _huffman.o _randtable.o
CLEANFILES +=	_bzlib.c _crctable.c _decompress.c _huffman.c _randtable.c

_bzlib.c: bzlib.c
	sed "s|bzlib_private\.h|libstand_bzlib_private.h|" $^ > $@

_crctable.c: crctable.c
	sed "s|bzlib_private\.h|libstand_bzlib_private.h|" $^ > $@

_decompress.c: decompress.c
	sed "s|bzlib_private\.h|libstand_bzlib_private.h|" $^ > $@

_huffman.c: huffman.c
	sed "s|bzlib_private\.h|libstand_bzlib_private.h|" $^ > $@

_randtable.c: randtable.c
	sed "s|bzlib_private\.h|libstand_bzlib_private.h|" $^ > $@

CLEANFILES += libstand_bzlib_private.h
libstand_bzlib_private.h: bzlib_private.h
	sed -e 's|<stdlib.h>|"stand.h"|' $^ > $@

# decompression functionality from libz
CFLAGS += -DHAVE_MEMCPY -I$(LIB_BASE)/libz
SRCS +=	adler32.c crc32.c libstand_zutil.h libstand_gzguts.h
OBJS +=	adler32.o crc32.o

_infback.c: infback.c
	sed -e "s|zutil\.h|libstand_zutil.h|" \
	    -e "s|gzguts\.h|libstand_gzguts.h|" \
	    $^ > $@
_inffast.c: inffast.c
	sed -e "s|zutil\.h|libstand_zutil.h|" \
	    -e "s|gzguts\.h|libstand_gzguts.h|" \
	    $^ > $@
_inflate.c: inflate.c
	sed -e "s|zutil\.h|libstand_zutil.h|" \
	    -e "s|gzguts\.h|libstand_gzguts.h|" \
	    $^ > $@
_inftrees.c: inftrees.c
	sed -e "s|zutil\.h|libstand_zutil.h|" \
	    -e "s|gzguts\.h|libstand_gzguts.h|" \
	    $^ > $@
_zutil.c: zutil.c
	sed -e "s|zutil\.h|libstand_zutil.h|" \
	    -e "s|gzguts\.h|libstand_gzguts.h|" \
	    $^ > $@

SRCS +=	_infback.c _inffast.c _inflate.c _inftrees.c _zutil.c
OBJS +=	_infback.o _inffast.o _inflate.o _inftrees.o _zutil.o
CLEANFILES +=	_infback.c _inffast.c _inflate.c _inftrees.c _zutil.c

# depend on stand.h being able to be included multiple times
libstand_zutil.h: zutil.h
	sed -e 's|<fcntl.h>|"stand.h"|' \
	    -e 's|<stddef.h>|"stand.h"|' \
	    -e 's|<string.h>|"stand.h"|' \
	    -e 's|<stdio.h>|"stand.h"|' \
	    -e 's|<stdlib.h>|"stand.h"|' \
	    $^ > $@

libstand_gzguts.h: gzguts.h
	sed -e 's|<fcntl.h>|"stand.h"|' \
	    -e 's|<stddef.h>|"stand.h"|' \
	    -e 's|<string.h>|"stand.h"|' \
	    -e 's|<stdio.h>|"stand.h"|' \
	    -e 's|<stdlib.h>|"stand.h"|' \
	    $^ > $@

CLEANFILES += libstand_zutil.h libstand_gzguts.h

# io routines
SRCS +=	closeall.c dev.c ioctl.c nullfs.c stat.c \
	fstat.c close.c lseek.c open.c read.c write.c readdir.c

OBJS +=	closeall.o dev.o ioctl.o nullfs.o stat.o fstat.o close.o lseek.o \
	open.o read.o write.o readdir.o

# network routines
SRCS +=	arp.c ether.c inet_ntoa.c in_cksum.c net.c udp.c netif.c rpc.c
OBJS +=	arp.o ether.o inet_ntoa.o in_cksum.o net.o udp.o netif.o rpc.o

# network info services:
SRCS +=	bootp.c rarp.c bootparam.c
OBJS +=	bootp.o rarp.o bootparam.o

# boot filesystems
SRCS +=	ufs.c
SRCS += nfs.c
SRCS += cd9660.c
SRCS += tftp.c
SRCS += gzipfs.c
SRCS += bzipfs.c
SRCS += dosfs.c
OBJS +=	ufs.o
OBJS +=	nfs.o
OBJS +=	cd9660.o
OBJS +=	tftp.o
OBJS +=	gzipfs.o
OBJS += bzipfs.o
OBJS += dosfs.o
#
