#

CC=	$(GCC_ROOT)/bin/gcc

all: lib

CPPFLAGS= -DSTAND -I. -I.. -I../../../../include -I../../../../lib/libstand
CPPFLAGS += -I../../..  -I$(FICLDIR) -I../../common
CFLAGS= -O2 -Wall -nostdinc

CFLAGS +=	-ffreestanding
CFLAGS +=	-mno-mmx -mno-3dnow -mno-sse -mno-sse2 -mno-sse3 -msoft-float
CFLAGS +=	-std=gnu99

OBJECTS= dictionary.o system.o fileaccess.o float.o double.o prefix.o search.o softcore.o stack.o tools.o vm.o primitives.o unix.o utility.o hash.o callback.o word.o loader.o
HEADERS= ficl.h ficlplatform/unix.h
#

LIB = ar cr
RANLIB = ranlib

MAJOR = 4
MINOR = 1.0

ficl: main.o $(HEADERS) libficl.a
	$(CC) $(LDFLAGS) main.o -o ficl -L. -lficl -lm

lib: machine x86 libficl.a

# static library build
libficl.a: $(OBJECTS)
	$(AR) $(ARFLAGS) libficl.a $(OBJECTS)
	$(RANLIB) libficl.a

machine:
	ln -sf ../../../$(MACHINE)/include machine

x86:
	ln -sf ../../../x86/include x86

# depend explicitly to help finding source files in another subdirectory,
# and repeat commands since gmake doesn't understand otherwise
ansi.o: ficlplatform/ansi.c $(HEADERS)
	$(CC) $(CFLAGS) $(CPPFLAGS) -c -o $@ $<
unix.o: ficlplatform/unix.c $(HEADERS)
	$(CC) $(CFLAGS) $(CPPFLAGS) -c -o $@ $<

#
#       generic object code
#
.SUFFIXES: .cxx .cc .c .o

.c.o:
	$(CC) $(CFLAGS) $(CPPFLAGS) -c -o $@ $<

.cxx.o:
	$(CPP) $(CXXFLAGS) $(CPPFLAGS) -c -o $@ $<

.cc.o:
	$(CPP) $(CXXFLAGS) $(CPPFLAGS) -c -o $@ $<
#
#       generic cleanup code
#
clobber clean:	FRC
	$(RM) *.o *.a libficl.* ficl machine x86
